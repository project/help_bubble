Description
-----------

Help Bubble module provides bubble popup which shows information about the field on which help is enabled. The medium of information about the field would be provided via video or documents created for the respective field.
